# README #

TorahCast est une application de gestion et de pilotage de salle de conférence basée sur la plateforme zoom.us

### What is this repository for? ###

* Quick summary
__Version__
* 0.9.0 : version initiale. 
* 0.9.1 : 
	* Mise à jour de la page de recherche (par mot / *). 
	* Ajout de la notion d'Etat (programmé, annulé et reporté).
	* Ajout de la localisation des dates.
	* Ajout de la notion d'abonnement.
	* Gestion du pannier.
* 0.9.2 :
	* Mise à jour de l'index sur la table collection "Conference"
	* Correction du controlleur, fonction ajouterConference : modification du set de l'Etat (PROGRAMMEE --> PROGRAMME).
	* Mise à jour de la vue rechercher pour le mediatype small.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact