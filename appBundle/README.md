appBundle
=========

A Symfony project created on February 17, 2018, 8:38 pm.

# Projet TorahCast

Cette application a été développée par l'Assocation Lilmod & Lelamed (c) 2018

Les crédits vont à Laurent HADJADJ.

# Version

* 0.9.3
 ** Remplacement de la police lilmod par des objet SVG ;
 ** Pris en compte du RGPD ;
 ** prise en compte de multi-conférencier (plusieurs clés zoom) ;