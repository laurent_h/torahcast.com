<?php

namespace TorahCastBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TorahCastBundle extends Bundle
{
  public function getParent()
    {
     return 'FOSUserBundle';
    }
}