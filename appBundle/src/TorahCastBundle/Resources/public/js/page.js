/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


/* Fonction de retour zu début de la page */
$(document).ready(function () {
  var duration = 500;
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      // Si un défillement de 100 pixels ou plus.
      // Ajoute le bouton
      $('.pageUP').fadeIn(duration);
    } else {
      // Sinon enlève le bouton
      $('.pageUP').fadeOut(duration);
    }
  });

  $('.pageUP').click(function (event) {
    // Un clic provoque le retour en haut animé.
    event.preventDefault();
    $('html, body').animate({scrollTop: 0}, duration);
    return false;
  })
});
