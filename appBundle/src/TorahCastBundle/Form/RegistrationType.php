<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;


class RegistrationType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      parent::buildForm($builder, $options);
      $builder
             ->add('membre', ChoiceType::class,
                  [
                   'choices'  => [
                                  'registration.intervenant' => 'INTERVENANT',
                                  'registration.participant' => 'PARTICIPANT',
                                  'registration.eleve' => 'ELEVE',
                                ],
                   'required' => true,
                   'label' => 'registration.membre',
                   'preferred_choices' => ['PARTICIPANT'],
                   'placeholder' => 'registration.membre_placeholder',
                   'translation_domain' => 'TorahCastUtilisateurBundle',
                   'error_bubbling' => true
                  ]
                  )

              ->add('genre', ChoiceType::class,
                  [
                   'choices'  => ['registration.monsieur' => 'MONSIEUR', 'registration.madame' => 'MADAME'],
                   'required' => true,
                   'label' => 'registration.genre',
                   'preferred_choices' => ['MONSIEUR'],
                   'placeholder' => 'registration.genre_placeholder',
                   'translation_domain' => 'TorahCastUtilisateurBundle',
                   'error_bubbling' => true
                  ]
                  )

              ->add('nom', TextType::class,
                   ['attr' => ['maxlength' => 50],
                    'required' => true,
                    'label' => 'registration.nom',
                    'attr' => ['placeholder' => 'registration.nom_placeholder'],
                    'translation_domain' => 'TorahCastUtilisateurBundle',
                    'trim' => true,
                    'error_bubbling' => false])

              ->add('prenom', TextType::class,
                    ['attr' => ['maxlength' => 50],
                     'required' => true,
                     'label' => 'registration.prenom',
                     'attr' => ['placeholder' => 'registration.prenom_placeholder'],
                     'translation_domain' => 'TorahCastUtilisateurBundle',
                     'trim' => true,
                     'error_bubbling' => false])

              ->add('telephone', TelType::class,
                    [
                     'required' => true,
                     'label' => 'registration.telephone',
                     'attr' => ['maxlength' => 20],
                     'attr' => ['placeholder' => 'registration.telephone_placeholder'],
                     'translation_domain' => 'TorahCastUtilisateurBundle',
                     'trim' => true,
                     'error_bubbling' => true])

              ->add('email', EmailType::class,
                      [
                       'required' => true,
                       'label' => 'form.email',
                       'attr' => ['placeholder' => 'registration.mel_placeholder'],
                       'translation_domain' => 'TorahCastUtilisateurBundle',
                       'trim' => true,
                       'error_bubbling' => true
                          ])

              ->add('timezone', ChoiceType::class,
                  [
                   'choices'  => ['registration.paris' => 'Europe/Paris', 'registration.jerusalem' => 'Asia/Jerusalem'],
                   'required' => true,
                   'label' => 'registration.timezone',
                   'preferred_choices' => ['Europe/Paris'],
                   'placeholder' => 'registration.timezone_placeholder',
                   'translation_domain' => 'TorahCastUtilisateurBundle',
                   'error_bubbling' => true
                  ])

              ->add('ip', HiddenType::class)
              ->add('username', HiddenType::class);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

        /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'torahcast_user_registration';
    }


 }
