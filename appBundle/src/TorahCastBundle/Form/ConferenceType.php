<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //parent::buildForm($builder, $options);
      $value = $options['listeTheme'];
      //print_r($value);
      //print_r(array_keys($value));
      //exit;
      $builder
                ->add('intervenant', TextType::class,
                        ['required' => true,
                         'attr' => array ('maxlength' => 26),
                         'attr' => array('placeholder' => 'conference.intervenant_placeholder'),
                         'label' => 'conference.intervenant',
                         'invalid_message' => 'conference.intervenant_invalid',
                         'translation_domain' => 'TorahCastBundle',
                         'trim' => true,
                        ])
                ->add('titre', TextType::class,
                        ['required' => true,
                         'attr' => array ('maxlength' => 56),
                         'attr' => array('placeholder' => 'conference.titre_placeholder'),
                         'label' => 'conference.titre',
                         'invalid_message' => 'conference.titre_invalid',
                         'translation_domain' => 'TorahCastBundle',
                         'trim' => true,
                        ])
                ->add('start', TextType::class,
                        ['required' => true,
                         'attr' => ['placeholder' => 'conference.start_placeholder', 'class'=>'js-datepicker'],
                         'label' => 'conference.start',
                         'invalid_message' => 'conference.start_invalid',
                         'translation_domain' => 'TorahCastBundle',
                         'trim' => true,
                         ])
                ->add('duree', TextType::class,
                        ['required' => false,
                         'attr' => array('placeholder' => 'conference.duree_placeholder', 'class'=>'js-timepicker'),
                         'label' => 'conference.duree',
                         'invalid_message' => 'conference.duree_invalid',
                         'translation_domain' => 'TorahCastBundle',
                         'trim' => true,
                         ])

                ->add('participant', ChoiceType::class,
                       [
                        'choices'  => [
                                  'conference.homme' => 'HOMME',
                                  'conference.femme' => 'FEMME',
                                  'conference.enfant' => 'ENFANT',
                                ],
                        'required' => true,
                        'label' => 'conference.participant',
                        'preferred_choices' => ['HOMME'],
                        'placeholder' => 'conference.participant_placeholder',
                        'translation_domain' => 'TorahCastBundle',
                        'error_bubbling' => true
                      ])

                ->add('langue', ChoiceType::class,
                  [
                   'choices'  => [
                                  'conference.fr' => 'fr',
                                  'conference.en' => 'en',
                                  'conference.he' => 'none',
                                ],
                   'label' => 'conference.langue',
                   'preferred_choices' => ['FR'],
                   'placeholder' => 'conference.langue_placeholder',
                   'translation_domain' => 'TorahCastBundle',
                   'error_bubbling' => true
                  ])

                ->add('theme', ChoiceType::class, //['choices'=>$value],
                     [
                      'choices' => $value,
                      'label' => 'conference.theme',
                      'placeholder' => 'conference.theme_placeholder',
                      'translation_domain' => 'TorahCastBundle',
                      'error_bubbling' => true
                     ])

                ->add('autreTheme', TextType::class,
                     ['required' => false,
                      'attr' => array ('maxlength' => 56),
                      'attr' => array('placeholder' => 'conference.autreTheme_placeholder'),
                      'label' => 'conference.autreTheme',
                      'invalid_message' => 'conference.autreTheme_invalid',
                      'translation_domain' => 'TorahCastBundle',
                      'trim' => true
                      ])

                ->add('timezone', ChoiceType::class,
                    [
                     'choices'  => ['conference.paris' => 'Europe/Paris', 'conference.jerusalem' => 'Asia/Jerusalem'],
                     'required' => true,
                     'label' => 'conference.timezone',
                     'preferred_choices' => ['Europe/Paris'],
                     'placeholder' => 'conference.timezone_placeholder',
                     'translation_domain' => 'TorahCastBundle',
                     'error_bubbling' => true
                    ])

               ->add('valider', SubmitType::class,
                        array('attr' => ['class' => 'button valider large expanded'],
                              'label'  => 'conference.valider',
                              'translation_domain' => 'TorahCastBundle',
                              ))
               ->add('effacer', ResetType::class,
                        array('attr' => ['class' => 'button effacer large expanded'],
                              'label'  => 'conference.effacer',
                              'translation_domain' => 'TorahCastBundle',
                              ));

        }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array('listeTheme' => null,));
    }

    }

 /*
  * m = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($video);
    $dm->flush();
  */