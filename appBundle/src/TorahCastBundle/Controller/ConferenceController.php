<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Controller;

use DateInterval;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TorahCastBundle\Document\Conference;
use TorahCastBundle\Document\Erreur;
use TorahCastBundle\Form\ConferenceType;


class ConferenceController extends Controller
{
    /**
     * @Route("/conference/ajouter", name="ajouterConference", options = { "utf8": true })
     */
    public function ajouterConferenceAction(Request $request)
    {
     if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     $cnx = $this->get('doctrine_mongodb')->getRepository('TorahCastBundle:Conference');

     //récupération et construction de la liste des theme
     $rq = $cnx->createQueryBuilder()->distinct('theme')
                                     ->select('theme')
                                     ->getQuery()
                                     ->debug();

     if (is_null($rq['query'])==false)
       {
        $ajouterConference= new Conference();
        $form=$this->createForm(ConferenceType::class, $ajouterConference, ['listeTheme'=>['Hassidout']]);
       }
     else
     {
      $i=0;
      foreach ($rq as $value) {$i++; $theme[$value]=$value;}
      asort($theme);
      $ajouterConference= new Conference();
      $form=$this->createForm(ConferenceType::class, $ajouterConference, ['listeTheme'=>$theme]);
     }

     // Gestion du formulaire
     $form->handleRequest($request);

     if ($form->isSubmitted() && $form->isValid()) {
         $ipAdresse=$this->container->get('request_stack')->getCurrentRequest()->getClientIp();

         $convertDateTime = $this->container->get('convertdatetime');

         $start=$form->get('start')->getData();
         $tempoStart=explode(" ",$start);
         $localDate=$tempoStart[0]."T".$tempoStart[1].":00";
         $dateTimezone=$convertDateTime->local2timezone($localDate,$form->get('timezone')->getData());

         $dateStart=$dateTimezone;
         //Ajoute la durée à la date de démarrage
         $duree=$form->get('duree')->getData();
         $time=explode(":",$duree);
         $dateEnd=$dateTimezone->add(new DateInterval('PT'.$time[0].'H'.$time[1].'M'));

         /* $dateTimezone
          * object(DateTime)#1090 (3)
          * { ["date"]=> string(26) "2018-04-21 23:30:00.000000"
          *   ["timezone_type"]=> int(3)
          *   ["timezone"]=> string(14) "Asia/Jerusalem" }
          */

         $timezoneCode="fr_FR";
         if ($form->get('timezone')->getData()=='Europe/Paris'){$timezoneCode="fr_FR";}
         if ($form->get('timezone')->getData()=='Asia/Jerusalem'){$timezoneCode="he_HE";}
         $ajouterConference->setTimezoneCode($timezoneCode);

         $ajouterConference->setStart($dateStart);
         $ajouterConference->setEnd($dateEnd);

         $ajouterConference->setIp($ipAdresse);
         $ajouterConference->setEmailCanonical($this->getUser()->getEmailCanonical());
         $ajouterConference->setStatut(true);
         $ajouterConference->setEtat('PROGRAMME');

          #j'enregistre une conference sur zoom.us
          //Conversion de la date de démarage au format GMT;
          //Convertion de la durée en minutes
          $minutes=$time[0]*60+$time[1];
          $tempo = [
                    "topic" => $form->get('titre')->getData(),
                    "type" => 2,
                    "start_time"=>$localDate,
                    "duration"=> $minutes,
                    "timezone"=>$form->get('timezone')->getData(),
                    "password"=> hash('adler32', microtime(true) . rand()),
                    "agenda"=>$form->get('theme')->getData(),
                    "settings"=>["mute_upon_entry"=>true, "join_before_host"=> false,]
                  ];

          $data = json_encode($tempo);
          /*
           * string(207) "
           *  {"topic":"test","type":2,
           *   "start_time":"2018-04-21T23:00:00",
           *   "duration":30,"timezone":"Asia\/Jerusalem",
           *   "password":"420c0518",
           *   "agenda":"Hassidout",
           *   "settings":{"mute_upon_entry":true,"join_before_host":false}}"
           */

         $zoomus = $this->container->get('zoomus');
         $createMeetings=$zoomus->createMeetings($this->getUser()->getEmailCanonical(),$data);


         $createMeetingsArray=[];
         $message="OK";

         foreach ($createMeetings as $key => $value)
           {
            switch ($key) {
                case "code":
                                  if ($key > "200") {$message=$createMeetings->message;}
                                  break;
                case "start_time":
                                  $createMeetingsArray[$key]=$value;
                                  break;
                case "created_at":
                                  $createMeetingsArray[$key]=$value;
                                  break;
                case "start_url":
                                  $createMeetingsArray[$key]=$value;
                                  break;
                case "join_url":
                                  $createMeetingsArray[$key]=$value;
                                  break;
                case "id":
                                  $createMeetingsArray[$key]=$value;
                                  break;

             }
           }
          if ($message!="OK")
              {
                $ajouterErreur = new Erreur();
                $ajouterErreur->setType('ADD');
                $ajouterErreur->setTimezone($form->get('timezone')->getData());
                $ajouterErreur->setTimezoneCode($timezoneCode);
                $ajouterErreur->setIntervenant($form->get('intervenant')->getData());
                $ajouterErreur->setTitre($form->get('titre')->getData());

                $ajouterErreur->setStart($dateTimezone);
                $ajouterErreur->setEnd($dateEnd);
                $ajouterErreur->setDuree($form->get('duree')->getData());
                $ajouterErreur->setTheme($form->get('theme')->getData());
                $ajouterErreur->setAutreTheme($form->get('autreTheme')->getData());
                $ajouterErreur->setLangue($form->get('langue')->getData());
                $ajouterErreur->setParticipant($form->get('paticipant')->getData());

                $ajouterErreur->setIp($ipAdresse);
                $ajouterErreur->setEmailCanonical($this->getUser()->getEmailCanonical());
                $ajouterErreur->setStatut(true);
                $ajouterErreur->setErreur($message);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($ajouterErreur);
                $dm->flush();

                $this->get('session')->getFlashBag()->add('alert', 'Oops, la conférence n\'a pas été enregistrée. Merci de contacter un responsable.');
                return $this->redirect($this->generateUrl('ajouterConference'));
              }

         $ajouterConference->setZoomUsStartTime($createMeetingsArray['start_time']);
         $ajouterConference->setZoomUsCreatedAt($createMeetingsArray['created_at']);
         $ajouterConference->setZoomUsStartUrl($createMeetingsArray['start_url']);
         $ajouterConference->setZoomUsJoinUrl($createMeetingsArray['join_url']);
         $ajouterConference->setZoomUsPassword($tempo['password']);
         $ajouterConference->setZoomUsId($createMeetingsArray['id']);

         $dm = $this->get('doctrine_mongodb')->getManager();
         $dm->persist($ajouterConference);
         $dm->flush();

         $this->get('session')->getFlashBag()->add('notice', 'La conférence a été enregistrée. ');

         return $this->redirect($this->generateUrl('ajouterConference'));
      }

     return $this->render('TorahCastBundle:Administration:conference_ajouter.html.twig',
              ['form'=>$form->createView(),
               'version' => $this->getParameter('version'),
               'dateCopyright'=>\date('Y'),]);
    }

    /**
     * @Route("/conference/verification", name="verificationConference", options = { "utf8": true })
     */
    public function verificationConferenceAction(Request $request)
    {
      if (!$this->getUser())
      {
         return $this->redirectToRoute('fos_user_security_login');
      }

    // processus permettant de vérifier la dipsonibilité d'une salle

     $salle=$this->getParameter('salle');

     $convertDateTime = $this->container->get('convertdatetime');
     $date = new \DateTime();
     $localDate = $date->format('Y-m-d H:i:s');

     $timezone=$this->getUser()->getTimezone();

     $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

     $cnx = $this->get('doctrine_mongodb')->getRepository('TorahCastBundle:Conference');
     $query = $cnx->createQueryBuilder()
                  ->field('end')->gte($dateTimezone)
                  ->getQuery()
                  ->execute()
                  ->count();

     $salleDisponible=$salle-$query;
     $pourcentage=($salleDisponible/$salle)*100;

     switch ($salleDisponible) {
      case $salle:
        $couleur="green";
        break;
      case 2:
        $couleur="orange";
        break;
      case 1:
        $couleur="red";
        break;
      default:
        $couleur="green";
     }
     $response = new JsonResponse();
     $response->setData(['salle'=>$salle,
                         'salleDisponible'=>$salleDisponible,
                         'pourcentage'=>$pourcentage,
                         'couleur'=>$couleur,
                         'date'=>$date,]);
     return $response;
    }

    /**
     * @Route("/conference/rechercher", name="rechercherConference", options = { "utf8": true })
     */
    public function rechercherConferenceAction()
    {
     if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     return $this->render('TorahCastBundle:Plannification:conference.rechercher.html.twig',
              ['version' => $this->getParameter('version'),
               'dateCopyright'=>\date('Y'),
              ]);
    }

     /**
     * @Route("/conference/afficher", name="afficherConference", options = { "utf8": true })
     */
    public function afficherConferenceAction()
    {
     if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     $convertDateTime = $this->container->get('convertdatetime');
     $date = new \DateTime();
     $localDate = $date->format('Y-m-d H:i:s');

     $timezone=$this->getUser()->getTimezone();

     $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

     $dm = $this->get('doctrine_mongodb')->getManager();
     $count = $dm->createQueryBuilder('TorahCastBundle:Conference')
                  ->field('start')->gt($dateTimezone)
                  ->field('statut')->equals(true)
                  ->getQuery()->execute()->count();

     if ($count==0) {  $data=""; }
      else
      {
      $data = $dm->createQueryBuilder('TorahCastBundle:Conference')
                 ->field('start')->gt($dateTimezone)
                 ->field('emailCanonical')->equals($this->getUser()->getEmailCanonical())
                 ->field('statut')->equals(true)
                 ->select('id', 'titre', 'start', 'duree')
                 ->limit(20)
                 ->sort('start', 'asc')
                 ->getQuery()
                 ->execute();
      }

      //todo a changer par le timzeone de la conférence
      switch ($this->getUser()->getTimezone()) {
        case "Europe/Paris": $pays="france";
          break;
        case "Asia/Jerusalem": $pays="israel";
          break;
        default:
        break;
      }

      return $this->render('TorahCastBundle:Administration:conference_afficher.html.twig',
              ['version' => $this->getParameter('version'),
               'timezoneCode'=>$this->getUser()->getTimezoneCode(),
               'pays'=>$pays,
               'dateCopyright'=>\date('Y'),
               'count'=>$count,
               'data'=>$data,
              ]);
    }


    /**
    * @Route("/conference/supprimer", name="supprimerConference", options = { "utf8": true })
    */
    public function supprimerConferenceAction(Request $request)
    {
     if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     $id = $request->get('id');

     $ipAdresse=$this->container->get('request_stack')->getCurrentRequest()->getClientIp();

     $convertDateTime = $this->container->get('convertdatetime');
     $date = new \DateTime();
     $localDate = $date->format('Y-m-d H:i:s');

     $timezone=$this->getUser()->getTimezone();

     $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

     $dm = $this->get('doctrine_mongodb')->getManager();
     $data = $dm->createQueryBuilder('TorahCastBundle:Conference')
                 ->field('id')->equals($id)
                 ->field('statut')->equals(true)
                 ->select('zoomUsId' )
                 ->hydrate(false)
                 ->getQuery()
                 ->execute();
     $meetingId=0;
     foreach ($data as $v1)
       {
        foreach ($v1 as $v2)
          {
          $meetingId=$v2;
          }
       }

      $zoomus = $this->container->get('zoomus');
      $deleteMeeting=$zoomus->deleteMeetings($meetingId);

      $message="OK";

      foreach ($deleteMeeting as $key => $value)
           {
            //code: 3001 message: "La réunion 411899677 est introuvable ou a expiré."
            if ($value=="3001") {$message=$deleteMeeting->message;}
           }

      if ($message!="OK")
        {
          $ajouterErreur = new Erreur();
          $ajouterErreur->setType('DELETE');
          $ajouterErreur->setIp($ipAdresse);
          $ajouterErreur->setEmailCanonical($this->getUser()->getEmailCanonical());
          $ajouterErreur->setdate($dateTimezone);
          $ajouterErreur->setErreur($message);

          $dm = $this->get('doctrine_mongodb')->getManager();
          $dm->persist($ajouterErreur);
          $dm->flush();

          $response = new JsonResponse();
          $response->setData(['reponse'=>$message]);
          return $response;
          }

        # Suppression logique
         $updateMeeting = $dm->createQueryBuilder('TorahCastBundle:Conference')
        // recherche la conference
        ->findAndUpdate()
        ->field('id')->equals($id)
        ->field('statut')->equals(true)

       // mise à jour la la conférence à statut=false
        ->field('date')->set($dateTimezone)
        ->field('statut')->set(false)
        ->getQuery()
        ->execute();

        //$message="OK";

        $response = new JsonResponse();
        $response->setData(['reponse'=>$message]);
        return $response;
    }

   /**
    * @Route("/conference/rejoindre", name="rejoindreConference", options = { "utf8": true })
    */
    public function rejoindreConferenceAction(Request $request)
    {
     if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     $id = $request->get('id');

     $qui=$this->getUser()->getMembre();

     $dm = $this->get('doctrine_mongodb')->getManager();

     if ($qui=="INTERVENANT")
     {
       $data = $dm->createQueryBuilder('TorahCastBundle:Conference')
                 ->field('id')->equals($id)
                 ->field('statut')->equals(true)
                 ->select('$zoomUsStartUrl' )
                 ->hydrate(false)
                 ->getQuery()
                 ->execute();
     }
     else
     { //Je suis un PARTICIPANT ou un ELEVE
       $data = $dm->createQueryBuilder('TorahCastBundle:Conference')
                 ->field('id')->equals($id)
                 ->field('statut')->equals(true)
                 ->select('zoomUsJoinUrl' )
                 ->hydrate(false)
                 ->getQuery()
                 ->execute();
     }

     $joinUrl=0;
     foreach ($data as $v1)
       {
        foreach ($v1 as $v2)
          {
            $joinUrl=$v2;
          }
       }

       $response = new JsonResponse();
       $response->setData(['reponse'=>$joinUrl]);
       return $response;
     }

   /**
    * @Route("/conference/liste", name="afficherMesConferences", options = { "utf8": true })
    */
    public function afficherMesConferencesAction()
    {
      if (!$this->getUser())
     {
       return $this->redirectToRoute('fos_user_security_login');
     }

     $convertDateTime = $this->container->get('convertdatetime');
     $date = new \DateTime();
     $localDate = $date->format('Y-m-d H:i:s');

     $timezone=$this->getUser()->getTimezone();

     $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

     $dm = $this->get('doctrine_mongodb')->getManager();
     $count = $dm->createQueryBuilder('TorahCastBundle:Abonnement')
                  ->field('date')->gt($dateTimezone)
                  ->field('emailCanonical')->equals($this->getUser()->getEmailCanonical())
                  ->field('statut')->equals(true)
                  ->getQuery()->execute()->count();

     if ($count==0) {  $data=""; }
      else
      {
       $dm = $this->get('doctrine_mongodb')->getManager();
       $data = $dm->createQueryBuilder('TorahCastBundle:Abonnement')
                 ->field('date')->gt($dateTimezone)
                 ->field('emailCanonical')->equals($this->getUser()->getEmailCanonical())
                 ->field('statut')->equals(true)
                 ->select('titre', 'theme','intervenant','participant','langue', 'date', 'duree', 'zoomUsJoinUrl', 'etat')
                 ->sort('start', 'asc')
                 ->getQuery()
                 ->execute();
      }

      switch ($this->getUser()->getTimezone()) {
        case "Europe/Paris": $pays="france";
          break;
        case "Asia/Jerusalem": $pays="israel";
          break;
        default:
        break;
      }

      return $this->render('TorahCastBundle:Plannification:conference.affiche.html.twig',
              ['version' => $this->getParameter('version'),
               'timezoneCode'=>$this->getUser()->getTimezoneCode(),
               'pays'=>$pays,
               'dateCopyright'=>\date('Y'),
               'count'=>$count,
               'data'=>$data,
              ]);
      }

/***** Fonction private de gestion des mises à jour de l'état des conférence
 *
 */
 private function updateConference()
   {
   /**************** Boîte de refoua chelema **********************************/
    $cnx= $this->get('doctrine_mongodb')->getRepository('TorahCastBundle:Conference');
    $dm = $this->get('doctrine_mongodb')->getManager();

   # Suppression logique
   $updateMeeting = $dm->createQueryBuilder('TorahCastBundle:Conference')
        // recherche la conference
        ->findAndUpdate()
        ->field('id')->equals($id)
        ->field('statut')->equals(true)

       // mise à jour la la conférence à statut=false
        ->field('date')->set($dateTimezone)
        ->field('statut')->set(false)
        ->getQuery()
        ->execute();

     return;
 }

  }
