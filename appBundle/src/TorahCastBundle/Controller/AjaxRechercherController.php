<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxRechercherController extends Controller
{
    /**
     *@Route("/rechercher", name="Rechercher", options = { "utf8": true })
    */
     public function rechercheAction(Request $request)
    {
      if (!$this->getUser()) {exit;}

      $mots = $request->get('mots');

      $convertDateTime = $this->container->get('convertdatetime');
      $date = new \DateTime();
      $localDate = $date->format('Y-m-d H:i:s');

      $timezone=$this->getUser()->getTimezone();

      $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

      $pays=[];

      $dm = $this->get('doctrine_mongodb')->getManager();
      if ($mots!="*")
      {
       $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->text($mots)
            ->selectMeta('score', 'textScore')
            ->limit(10)
            ->sort(array('$meta' => 'textScore'))
            ->getQuery()
            ->execute();
       $etoile=false;
      }
      else
      {
       $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->select('id', 'titre', 'theme', 'start', 'duree', 'intervenant', 'participant', 'langue', 'etat')
            ->limit(20)
            ->sort('start', 'asc')
            ->getQuery()
            ->execute();
       $etoile=true;
      }
      $nombreReponse=count($qb);

      if ($nombreReponse === 0)
        {
        $liste[0]=['MESSAGE'=>"Je n'ai pas trouvé de réponse."];
        $reponse=['liste'=>$liste];
        }

      /*********************************** Etoile ******************************/
      if ($nombreReponse != 0 and $etoile==false)
       {
         foreach ($qb as $value)
         {
          $id=$value->getId();
          $titre=$value->getTitre();
          $theme=$value->getTheme();
          $date=$value->getStart();
          $duree=$value->getDuree();
          $participant=$value->getParticipant();
          $intervenant=$value->getIntervenant();
          $langue=$value->getLangue();
          $etat=$value->getEtat();

          $localDate = $date->format('Y-m-d H:i:s');

          switch ($timezone) {
            case "Europe/Paris": $pays="france";
              break;
            case "Asia/Jerusalem": $pays="israel";
              break;
            default:
              break;
             }

          $liste[]=
                  [
                   'id'=>$id,
                   'titre'=>$titre,
                   'theme'=>$theme,
                   'date'=>$convertDateTime->local2timezone($localDate,$timezone),
                   'duree'=>$duree,
                   'intervenant'=>$intervenant,
                   'participant'=>$participant,
                   'langue'=>$langue,
                   'timezoneCode'=>$this->getUser()->getTimezoneCode(),
                   'pays'=>$pays,
                   'etat'=>$etat,
                  ];
          }
         $reponse=[$liste];
        }
        else
        {
         foreach ($qb as $value)
         {
          $id=$value->getId();
          $titre=$value->getTitre();
          $theme=$value->getTheme();
          $date=$value->getStart();
          $duree=$value->getDuree();
          $participant=$value->getParticipant();
          $intervenant=$value->getIntervenant();
          $langue=$value->getLangue();
          $etat=$value->getEtat();

          $localDate = $date->format('Y-m-d H:i:s');

          switch ($timezone) {
            case "Europe/Paris": $pays="france";
              break;
            case "Asia/Jerusalem": $pays="israel";
              break;
            default:
              break;
             }

          $liste[]=
                  [
                   'id'=>$id,
                   'titre'=>$titre,
                   'theme'=>$theme,
                   'date'=>$convertDateTime->local2timezone($localDate,$timezone),
                   'duree'=>$duree,
                   'intervenant'=>$intervenant,
                   'participant'=>$participant,
                   'langue'=>$langue,
                   'timezoneCode'=>$this->getUser()->getTimezoneCode(),
                   'pays'=>$pays,
                   'etat'=>$etat,
                  ];
          }
         $reponse=[$liste];

        }
      $response = new JsonResponse();
      $response->setData($this->renderView('TorahCastBundle:Plannification:conference.rechercher.liste.html.twig',
                                            ['reponse'=>$reponse,'max'=>$nombreReponse,'etoile'=>$etoile,]));
      return $response;
    }

 }