<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home", options = { "utf8": true })
     */
    public function indexAction()
    {
      return $this->render('TorahCastBundle::index.html.twig',
              ['version' => $this->getParameter('version'),
               'dateCopyright'=>\date('Y'),
              ]);
    }

    /**
     * @Route("/menu", name="menu", options = { "utf8": true })
     */
    public function menuAction()
    {
          return $this->render('TorahCastBundle:Plannification:menu.index.html.twig',
              ['version' => $this->getParameter('version'), 'dateCopyright'=>\date('Y'),]);

    }
}