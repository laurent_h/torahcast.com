<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Controller;

use DateTime;
use DateTimeZone;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TorahCastBundle\Document\Test;


class ZtestController extends Controller
{


   /**
    * @Route("/test01", name="test01", options = { "utf8": true })
    */
    public function test01Action()
    {
     $tempo=new DateTime();
     foreach ($tempo as $key=>$value) {
       $date[$key]=[$value];
     }
     print_r($date['date']);
     //print_r($date['timezone']);
     echo "<br />";
     print_r(gmdate($date['date']['0']));
     exit;
    }

   /**
    * @Route("/test02", name="test02", options = { "utf8": true })
    */
    public function test02Action()
    {
     $dm = $this->get('doctrine_mongodb')->getManager();
     $data = $dm->createQueryBuilder('TorahCastBundle:Conference')
                 ->field('id')->equals('5adcbc27a3e8880d35083e92')
                 ->field('statut')->equals(true)
                 ->select('zoomUsId' )
                 ->hydrate(false)
                 ->getQuery()
                 ->execute();
     $tempo=0;
     foreach ($data as $v1)
       {
        foreach ($v1 as $v2)
          {
          $tempo=$v2;
          }
       }

       echo $tempo;

     exit;
    }

   /**
    * @Route("/test03", name="test03", options = { "utf8": true })
    */
    public function test03Action()
    {
     $dm = $this->get('doctrine_mongodb')->getManager();

    date_default_timezone_set('Asia/Jerusalem');

    $currentDateTime =  "2018-04-18T16:00:00";
    echo "<b>Date saisie</b> : " . $currentDateTime . "<br /><br />";

    //Conversion de la date courante en date timezone
    $dateJerusalem = new DateTime($currentDateTime);
    $dateJerusalem->setTimezone( new DateTimeZone('Asia/Jerusalem'));
    echo "<b>Date Jerusalem </b>: " . $dateJerusalem->format('Y-m-d H:i:s') . "<br /><br />";

    //var_dump($dateJerusalem);
    //echo "<br /><br />";

    $newDate = new Test();
    $newDate->setDateLocale($currentDateTime);
    $newDate->setDateUTC($dateJerusalem);
    $dm->persist($newDate);
    $dm->flush();


    $data = $dm->createQueryBuilder('TorahCastBundle:Test')
       ->getQuery()
       ->execute();

    //$timezone = new DateTimeZone('Asia/Kolkata');
    foreach ($data as $value)
      {
        echo "<br /><br />Locale  " . $value->getDateLocale();
        echo "<br /><br />Paris   " . $value->getDateUTC()->setTimeZone(new DateTimeZone('Europe/Paris'))->format("Y-m-d H:i:s");
        echo "<br /><br />Jerusalem " . $value->getDateUTC()->setTimeZone(new DateTimeZone('Asia/Jerusalem'))->format("Y-m-d H:i:s");
        echo "<br /><br />"; var_dump($value);


      //print_r($value); echo "<br /><br />";

        //print_r($value->getDateUTC());

      }

    //var_dump($tempo);
    exit;
    }

   /**
    * @Route("/test04", name="test04", options = { "utf8": true })
    */
    public function test04Action()
    {
      $convertDateTime = $this->container->get('convertdatetime');
      $date = new DateTime();
      $localDate = $date->format('Y-m-d H:i:s');

      $timezone=$this->getUser()->getTimezone();

      $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

      $dm = $this->get('doctrine_mongodb')->getManager();
      $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->select('titre', 'theme', 'start', 'duree', 'intervenant', 'participant', 'langue')
            ->limit(20)
            ->sort('start', 'asc')
            ->getQuery()
            ->execute();

      $timezone=[];
            foreach ($qb as $value)
         {
          $titre=$value->getTitre();
          $theme=$value->getTheme();
          $date=$value->getStart();
          $duree=$value->getDuree();
          $participant=$value->getParticipant();
          $intervenant=$value->getIntervenant();

          foreach ($date as $key => $value) {$timezone=$value;}
          //todo a changer par le timzeone de la conférence
          switch ($timezone) {
            case "Europe/Paris": $pays="france";
              break;
            case "Asia/Jerusalem": $pays="israel";
              break;
            default:
              break;
             }

          $liste[]=
                  [
                   'titre'=>$titre,
                   'theme'=>$theme,
                   'date'=>$date,
                   'duree'=>$duree,
                   'intervenant'=>$intervenant,
                   'participant'=>$participant,
                   'pays'=>$pays,
                  ];
         }
      var_dump($liste);
      exit;
      }

   /**
    * @Route("/test05", name="test05", options = { "utf8": true })
    */
    public function test05Action()
    {
      $convertDateTime = $this->container->get('convertdatetime');
      $date = new DateTime();
      $localDate = $date->format('Y-m-d H:i:s');

      $timezone=$this->getUser()->getTimezone();

      $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

      $dm = $this->get('doctrine_mongodb')->getManager();
      $qb = $dm->createQueryBuilder('TorahCastBundle:Abonnement')
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->select('id', 'abonnement')
            ->getQuery();

      $nombreReponse=count($qb);

      if ($nombreReponse==0)
        {
          $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('id')->equals($id)
            ->select('id', 'titre', 'theme', 'start', 'duree', 'intervenant', 'participant', 'langue')
            ->limit(20)
            ->sort('start', 'asc')
            ->getQuery()
            ->execute();

          //je créé un nouvel enregistrement
        }

       exit;
    }

       /**
    * @Route("/test06", name="test06", options = { "utf8": true })
    */
    public function test06Action()
    {

            $convertDateTime = $this->container->get('convertdatetime');
      $date = new \DateTime();
      $localDate = $date->format('Y-m-d H:i:s');

      $timezone=$this->getUser()->getTimezone();

      $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

      $dm = $this->get('doctrine_mongodb')->getManager();

            $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('id')->equals('5ae1ecb4a3e8882bb8183682')
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->select('titre', 'theme', 'start', 'duree', 'intervenant', 'participant', 'langue', 'etat')
            ->getQuery();

         foreach ($qb as $value)
         {
         //var_dump($value);

           $titre=$value->getTitre();
          $theme=$value->getTheme();
          $date=$value->getStart();
          $duree=$value->getDuree();
          $participant=$value->getParticipant();
          $intervenant=$value->getIntervenant();
          $langue=$value->getLangue();
          $statut=$value->getStatut();
          $etat=$value->getEtat();
         }
         var_dump($titre);

         $dm = $this->get('doctrine_mongodb')->getManager();
         $qb = $dm->createQueryBuilder('TorahCastBundle:Abonnement')
            ->field('abonnementId')->equals('5ae1ecb4a3e8882bb8183682')
            ->field('statut')->equals(true)
            ->getQuery();

        $nombreReponse=count($qb);
        echo "<br />resultat :";
        var_dump($nombreReponse);

        //exit;
    }

  }
