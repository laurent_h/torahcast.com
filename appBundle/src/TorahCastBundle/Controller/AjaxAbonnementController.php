<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */

namespace TorahCastBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxAbonnementController extends Controller
{
    /**
     *@Route("/abonnement", name="abonnementConference", options = { "utf8": true })
    */
     public function abonnementAction(Request $request)
    {
      $id = $request->get('id');

      $ipAdresse=$this->container->get('request_stack')->getCurrentRequest()->getClientIp();

      $convertDateTime = $this->container->get('convertdatetime');
      $date = new \DateTime();
      $localDate = $date->format('Y-m-d H:i:s');

      $timezone=$this->getUser()->getTimezone();

      $dateTimezone=$convertDateTime->local2timezone($localDate,$timezone);

      $dm = $this->get('doctrine_mongodb')->getManager();
      $qb = $dm->createQueryBuilder('TorahCastBundle:Abonnement')
            ->field('abonnementId')->equals($id)
            ->field('statut')->equals(true)
            ->getQuery();

      $nombreReponse=count($qb);

      if ($nombreReponse != 0)
        {
        $reponse=["Vous avez déjà cette conférence dans votre pannier."];
        $statut="alert";
        }

      if ($nombreReponse == 0)
       {
        //On récupere les données de la conférence
        $qb = $dm->createQueryBuilder('TorahCastBundle:Conference')
            ->field('id')->equals($id)
            ->field('start')->gt($dateTimezone)
            ->field('statut')->equals(true)
            ->select('titre', 'theme', 'start', 'duree', 'intervenant', 'participant', 'langue', 'etat', 'statut', 'zoomUsJoinUrl')
            ->getQuery();

         foreach ($qb as $value)
         {
          $titre=$value->getTitre();
          $theme=$value->getTheme();
          $date=$value->getStart();
          $duree=$value->getDuree();
          $participant=$value->getParticipant();
          $intervenant=$value->getIntervenant();
          $langue=$value->getLangue();
          $statut=$value->getStatut();
          $etat=$value->getEtat();
          $joinUrl=$value->getZoomUsJoinUrl();
         }

         $ajoutAbonnement = new \TorahCastBundle\Document\Abonnement();
         $ajoutAbonnement->setAbonnementId($id);
         $ajoutAbonnement->setEmailCanonical($this->getUser()->getEmailCanonical());
         $ajoutAbonnement->setTitre($titre);
         $ajoutAbonnement->setTheme($theme);
         $ajoutAbonnement->setIp($ipAdresse);
         $ajoutAbonnement->setDate($date);
         $ajoutAbonnement->setDuree($duree);
         $ajoutAbonnement->setIntervenant($intervenant);
         $ajoutAbonnement->setParticipant($participant);
         $ajoutAbonnement->setLangue($langue);
         $ajoutAbonnement->setEtat($etat);
         $ajoutAbonnement->setStatut($statut);
         $ajoutAbonnement->setZoomUsJoinUrl($joinUrl);

         $dm = $this->get('doctrine_mongodb')->getManager();
         $dm->persist($ajoutAbonnement);
         $dm->flush();

         $reponse=["La conférence a été ajoutée."];
         $statut="success";
       }

      $response = new JsonResponse();
      $response->setData(['message'=>$reponse,'statut'=>$statut]);
      return $response;
    }

 }