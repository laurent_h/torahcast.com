<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="Abonnement")
 */
class Abonnement
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**  @MongoDB\Field(type="string")
     */
    protected $intervenant;

    /**  @MongoDB\Field(type="string") */
    protected $emailCanonical;

    /**  @MongoDB\Field(type="string")
     */
    protected $titre;

    /**  @MongoDB\Field(type="string")
    */
    protected $timezone;

    /**  @MongoDB\Field(type="string")
    */
    protected $timezoneCode;

    /**  @MongoDB\Field(type="string")
     */
    protected $duree;

    /**  @MongoDB\Field(type="string")
     */
    protected $theme;

    /**  @MongoDB\Field(type="string")
     */
    protected $langue;

    /**  @MongoDB\Field(type="string")
     */
    protected $participant;

    /**  @MongoDB\Field(type="string")*/
    protected $ip;

    /**  @MongoDB\Field(type="date")*/
    protected $date;

    /**  @MongoDB\Field(type="boolean")*/
    protected $statut;

    /**  @MongoDB\Field(type="string")*/
    protected $zoomUsJoinUrl;

    /**  @MongoDB\Field(type="string")*/
    protected $zoomUsPassword;

    /**  @MongoDB\Field(type="string")*/
    protected $abonnementId;

    /**  @MongoDB\Field(type="string")*/
    protected $etat;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intervenant
     *
     * @param string $intervenant
     * @return $this
     */
    public function setIntervenant($intervenant)
    {
        $this->intervenant = $intervenant;
        return $this;
    }

    /**
     * Get intervenant
     *
     * @return string $intervenant
     */
    public function getIntervenant()
    {
        return $this->intervenant;
    }

    /**
     * Set emailCanonical
     *
     * @param string $emailCanonical
     * @return $this
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;
        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return string $emailCanonical
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return $this
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * Get titre
     *
     * @return string $titre
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * Get timezone
     *
     * @return string $timezone
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set timezoneCode
     *
     * @param string $timezoneCode
     * @return $this
     */
    public function setTimezoneCode($timezoneCode)
    {
        $this->timezoneCode = $timezoneCode;
        return $this;
    }

    /**
     * Get timezoneCode
     *
     * @return string $timezoneCode
     */
    public function getTimezoneCode()
    {
        return $this->timezoneCode;
    }

    /**
     * Set duree
     *
     * @param string $duree
     * @return $this
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
        return $this;
    }

    /**
     * Get duree
     *
     * @return string $duree
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return $this
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
        return $this;
    }

    /**
     * Get theme
     *
     * @return string $theme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set langue
     *
     * @param string $langue
     * @return $this
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;
        return $this;
    }

    /**
     * Get langue
     *
     * @return string $langue
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set participant
     *
     * @param string $participant
     * @return $this
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
        return $this;
    }

    /**
     * Get participant
     *
     * @return string $participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * Get ip
     *
     * @return string $ip
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statut
     *
     * @param boolean $statut
     * @return $this
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * Get statut
     *
     * @return boolean $statut
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set zoomUsJoinUrl
     *
     * @param string $zoomUsJoinUrl
     * @return $this
     */
    public function setZoomUsJoinUrl($zoomUsJoinUrl)
    {
        $this->zoomUsJoinUrl = $zoomUsJoinUrl;
        return $this;
    }

    /**
     * Get zoomUsJoinUrl
     *
     * @return string $zoomUsJoinUrl
     */
    public function getZoomUsJoinUrl()
    {
        return $this->zoomUsJoinUrl;
    }

    /**
     * Set zoomUsPassword
     *
     * @param string $zoomUsPassword
     * @return $this
     */
    public function setZoomUsPassword($zoomUsPassword)
    {
        $this->zoomUsPassword = $zoomUsPassword;
        return $this;
    }

    /**
     * Get zoomUsPassword
     *
     * @return string $zoomUsPassword
     */
    public function getZoomUsPassword()
    {
        return $this->zoomUsPassword;
    }

    /**
     * Set abonnementId
     *
     * @param string $abonnementId
     * @return $this
     */
    public function setAbonnementId($abonnementId)
    {
        $this->abonnementId = $abonnementId;
        return $this;
    }

    /**
     * Get abonnementId
     *
     * @return string $abonnementId
     */
    public function getAbonnementId()
    {
        return $this->abonnementId;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return $this
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * Get etat
     *
     * @return string $etat
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
