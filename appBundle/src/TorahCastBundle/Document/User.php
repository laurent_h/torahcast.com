<?php
/*
 * Copyright 2018 Laurent HADJADJ <laurent.HADJADJ@lilmod-lelemaed.fr>.
 *
 * Licensed Crative Common 4.0 - CC-BY-SA
 * Vous pouvez otenir une copie de la licence à l'adresse suivante :
 *
 *      http://creativecommons.org/licenses/by-sa/4.0/
 *
 * TorahCast de Lilmod & Lelamed
 * est mis à disposition selon les termes de la licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International.
 * Fondé(e) sur une œuvre disppnble à l'adresse : https://bitbucket.org/laurent_h/torahcast/.
 */


namespace TorahCastBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="User")
 */

class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**  @MongoDB\Field(type="string") */
    protected $genre;

    /**  @MongoDB\Field(type="string") */
    protected $membre;

    /**  @MongoDB\Field(type="string") */
    protected $nom;

    /**  @MongoDB\Field(type="string") */
    protected $prenom;

     /**  @MongoDB\Field(type="string") */
    protected $facebookId;

     /**  @MongoDB\Field(type="string") */
    protected $googleId;

     /**  @MongoDB\Field(type="string") */
    protected $twitterId;

    /**  @MongoDB\Field(type="boolean") */
    protected $intervenant;

   /**  @MongoDB\Field(type="boolean") */
    protected $participant;

    /**  @MongoDB\Field(type="string")
         @Assert\Regex("/([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/")
     */
    protected $telephone;

   /**  @MongoDB\Field(type="string") */
    protected $timezone;

   /**  @MongoDB\Field(type="string") */
    protected $timezoneCode;

    /**  @MongoDB\Field(type="string") */
    protected $ip;


    /**
     * Set genre
     *
     * @param string $genre
     * @return $this
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
        return $this;
    }

    /**
     * Get genre
     *
     * @return string $genre
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set membre
     *
     * @param string $membre
     * @return $this
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
        return $this;
    }

    /**
     * Get membre
     *
     * @return string $membre
     */
    public function getMembre()
    {
        return $this->membre;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return $this
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get nom
     *
     * @return string $nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return $this
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string $prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return $this
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string $facebookId
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     * @return $this
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * Get googleId
     *
     * @return string $googleId
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Set twitterId
     *
     * @param string $twitterId
     * @return $this
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;
        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string $twitterId
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set intervenant
     *
     * @param boolean $intervenant
     * @return $this
     */
    public function setIntervenant($intervenant)
    {
        $this->intervenant = $intervenant;
        return $this;
    }

    /**
     * Get intervenant
     *
     * @return boolean $intervenant
     */
    public function getIntervenant()
    {
        return $this->intervenant;
    }

    /**
     * Set participant
     *
     * @param boolean $participant
     * @return $this
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
        return $this;
    }

    /**
     * Get participant
     *
     * @return boolean $participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * Get timezone
     *
     * @return string $timezone
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set timezoneCode
     *
     * @param string $timezoneCode
     * @return $this
     */
    public function setTimezoneCode($timezoneCode)
    {
        $this->timezoneCode = $timezoneCode;
        return $this;
    }

    /**
     * Get timezoneCode
     *
     * @return string $timezoneCode
     */
    public function getTimezoneCode()
    {
        return $this->timezoneCode;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * Get ip
     *
     * @return string $ip
     */
    public function getIp()
    {
        return $this->ip;
    }
}
