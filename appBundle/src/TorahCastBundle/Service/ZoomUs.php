<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TorahCastBundle\Service;

/**
 * Description of zoomController
 *
 * @author laurent.hadjadj
 */

use Firebase\JWT\JWT;


class ZoomUs {

    //function to generate JWT
    private function generateJWT() {
      //Zoom API credentials from https://developer.zoom.us/me/
      $key = '5UFIFdPvSJ6s6Sx8BywBuA';
      $secret = 'cTO3V42Kp5M3G68SF4xckBBYHaAvwY7zK6Qk';
      $token = [ "iss" => $key, "exp" => time() + 60];
      return JWT::encode($token, $secret);
    }


    public function getUsers() {
        //list users endpoint GET https://api.zoom.us/v2/users
    	$ch = curl_init('https://api.zoom.us/v2/user');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->generateJWT()]);
        $response = curl_exec($ch);
        return json_decode($response);
    }

    public function getListMeetings($userId) {
        //list metettings by users endpoint GET https://api.zoom.us/v2/users/{userId}/meetings
    	$ch = curl_init('https://api.zoom.us/v2/users/'.$userId.'/meetings');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->generateJWT()]);
        $response = curl_exec($ch);
        return json_decode($response);
    }

    public function createMeetings($userId, $data) {

        //list metettings by users endpoint post https://api.zoom.us/v2/users/{userId}/meetings
    	$ch = curl_init('https://api.zoom.us/v2/users/'.$userId.'/meetings');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        // add token to the authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                ['Authorization: Bearer ' . $this->generateJWT(),
                 'Content-Type: application/json',
                 'Content-Length: ' . strlen($data)
                 ]);

        $response = curl_exec($ch);

        return json_decode($response);
    }


    //$ curl https://api.zoom.us/v2/meetings/{meetingId} -X DELETE

    public function deleteMeetings($meetingId) {

//      $meetingId=$meetingId-1;

        //delete a conference DELETE https://api.zoom.us/v2/meetings/{meetingId}
    	$ch = curl_init('https://api.zoom.us/v2/meetings/'.$meetingId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->generateJWT()]);

        $response = curl_exec($ch);

        return json_decode($response);
    }

  }
