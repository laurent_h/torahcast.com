<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TorahCastBundle\Service;

/**
 * Description of zoomController
 *
 * @author laurent.hadjadj
 */

use DateTime;
use DateTimeZone;

class ConvertDateTime {

    public function local2Timezone($localDate, $timezone) {

    date_default_timezone_set($timezone);

    $date = new DateTime($localDate);
    $date->setTimezone( new DateTimeZone($timezone));

    //echo "<b>Date Jerusalem </b>: " . $date->format('Y-m-d H:i:s');

     return $date;
    }

  }
